package redis

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/mediocregopher/radix"
)

var (
	DB *radix.Pool
)

type RedisInfo struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Pool     int    `json:"pool"`
	Password string `json:"password"`
	Ttl      int    `json:"ttl"`
}

func ConnectRedis(o RedisInfo) {
	info := fmt.Sprintf("%s:%d", o.Host, o.Port)
	interval := radix.PoolPingInterval(60 * time.Second)
	var err error
	DB, err = radix.NewPool("tcp", info, o.Pool, interval)
	if err != nil {
		panic(err)
	}
}

//func GetHash(key string) (map[string]interface{}, error) {
func GetHash(key string, obj interface{}) (interface{}, error) {
	//resp := make(map[string]interface{})
	err := DB.Do(radix.Cmd(obj, "HGETALL", key))
	if err != nil {
		log.Println(err)
	}
	//return resp, err
	return obj, err
}

func Get(key string) (string, error) {
	resp := ""
	err := DB.Do(radix.Cmd(&resp, "GET", key))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func Gets(keys ...string) ([]string, error) {
	var resp []string
	err := DB.Do(radix.Cmd(&resp, "MGET", keys...))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func Set(key string, value string) (string, error) {
	resp := ""
	err := DB.Do(radix.Cmd(&resp, "SET", key, value))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func HmSet(key string, m map[string]interface{}) error {
	err := DB.Do(radix.FlatCmd(nil, "HMSET", key, m))
	if err != nil {
		log.Println(err)
	}
	return err
}

func HSet(key string, field string, val string) error {
	err := DB.Do(radix.Cmd(nil, "HSET", key, field, val))
	if err != nil {
		log.Println(err)
	}
	return err
}

func HGet(key string, field string) (string, error) {
	resp := ""
	err := DB.Do(radix.Cmd(&resp, "HGET", key, field))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func Search(pattern string) ([]string, error) {
	var resp []string
	err := DB.Do(radix.Cmd(&resp, "KEYS", pattern))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func ZAdd(setName string, index int, value string) (string, error) {
	var resp string
	err := DB.Do(radix.Cmd(&resp, "ZADD", setName, strconv.Itoa(index), value))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func ZCard(setName string) (int, error) {
	var resp string
	err := DB.Do(radix.Cmd(&resp, "ZCARD", setName))
	if err != nil {
		log.Println(err)
	}
	num, err := strconv.Atoi(resp)
	return num, err
}

func ZRange(key string) ([]string, error) {
	var resp []string
	err := DB.Do(radix.Cmd(&resp, "ZRANGE", key, "0", "-1"))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func GeoAdd(key string, long float64, lat float64, value string) (string, error) {
	resp := ""
	Long := fmt.Sprintf("%f", long)
	Lat := fmt.Sprintf("%f", lat)
	err := DB.Do(radix.Cmd(&resp, "GEOADD", key, Long, Lat, value))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}

func GeoDist(key string, member1 string, member2 string, unit string) (string, error) {
	resp := ""
	err := DB.Do(radix.Cmd(&resp, "GEODIST", key, member1, member2, unit))
	if err != nil {
		log.Println(err)
	}
	return resp, err
}
